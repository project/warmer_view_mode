<?php

namespace Drupal\warmer_view_mode\Plugin\warmer;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Error;
use Drupal\warmer\Plugin\WarmerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The cache warmer for the built-in entity cache.
 *
 * @Warmer(
 *   id = "view_mode",
 *   label = @Translation("View Mode"),
 *   description = @Translation("Loads view modes from the selected entity types & bundles to warm the render cache.")
 * )
 */
final class ViewModeWarmer extends WarmerPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The in-memory static entity cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  private MemoryCacheInterface $entityMemoryCache;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  private EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * The bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  private EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * The list of all item IDs for all entities in the system.
   *
   * Consists of <entity-type-id>:<entity-id>.
   *
   * @var array
   */
  private array $itemIds = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    assert($instance instanceof ViewModeWarmer);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setEntityMemoryCache($container->get('entity.memory_cache'));
    $instance->setEntityDisplayRepository($container->get('entity_display.repository'));
    $instance->setRenderer($container->get('renderer'));
    $instance->setLogger($container->get('logger.factory'));
    $instance->setBundleInfo($container->get('entity_type.bundle.info'));
    return $instance;
  }

  /**
   * Injects the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Injects the entity memory cache.
   *
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   */
  public function setEntityMemoryCache(MemoryCacheInterface $memory_cache) {
    $this->entityMemoryCache = $memory_cache;
  }

  /**
   * Injects the entity display repository.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   */
  public function setEntityDisplayRepository(EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * Injects the render.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function setRenderer(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Injects the logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory service.
   */
  public function setLogger(LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->logger = $logger_channel_factory->get('warmer');
  }

  /**
   * Injects the bundle info.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity bundle info service.
   */
  public function setBundleInfo(EntityTypeBundleInfoInterface $bundle_info) {
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = []): array {
    $ids_per_type = array_reduce($ids, function ($carry, $id) {
      [$entity_type_id, $entity_id, $view_mode_id] = explode(':', $id);
      if (!array_key_exists($entity_type_id, $carry)) {
        $carry[$entity_type_id] = [];
      }
      if (!array_key_exists($view_mode_id, $carry[$entity_type_id])) {
        $carry[$entity_type_id][$view_mode_id] = [];
      }
      $carry[$entity_type_id][$view_mode_id][] = $entity_id;
      return $carry;
    }, []);
    $output = [];
    foreach ($ids_per_type as $entity_type_id => $view_modes) {
      foreach ($view_modes as $view_mode_id => $entity_ids) {
        try {
          $entities = $this->entityTypeManager
            ->getStorage($entity_type_id)
            ->loadMultiple($entity_ids);
          foreach ($entities as $entity) {
            $output[] = [
              $entity_type_id,
              $view_mode_id,
              $entity,
            ];
          }
        }
        catch (PluginException | DatabaseExceptionWrapper $exception) {
          Error::logException($this->logger, $exception);
        }
      }

      // \Drupal\Core\Entity\EntityStorageBase::buildCacheId() is protected,
      // so we blindly reset the whole static cache instead of specific IDs.
      $this->entityMemoryCache->deleteAll();
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function warmMultiple(array $items = []): ?int {
    foreach ($items as $item) {
      $entity_type_id = $item[0];
      $view_mode_id = $item[1];
      $entity = $item[2];
      $build = $this->entityTypeManager->getViewBuilder($entity_type_id)->view($entity, $view_mode_id);
      $this->renderer->renderRoot($build);
    }

    return count($items);
  }

  /**
   * {@inheritdoc}
   */
  public function buildIdsBatch($cursor): array {
    $configuration = $this->getConfiguration();
    $view_modes = $this->getViewModesFromConfig($configuration);

    if (empty($this->itemIds) && !empty($view_modes)) {
      $this->itemIds = [];
      foreach ($view_modes as $entity_type_id => $bundles) {
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        $bundle_key = $entity_type->getKey('bundle');
        $id_key = $entity_type->getKey('id');
        if (array_key_exists('published', $configuration)
          && $configuration['published']
          && $entity_type instanceof EntityPublishedInterface
        ) {
          $onlyPublished = TRUE;
        }
        else {
          $onlyPublished = FALSE;
        }
        $entityStorage = $this->entityTypeManager->getStorage($entity_type_id);
        foreach ($bundles as $bundle_id => $view_modes) {

          $query = $entityStorage->getQuery();
          $query->accessCheck(FALSE);
          if ($id_key) {
            $query->sort($id_key);
          }
          if ($bundle_key) {
            $query->condition($bundle_key, $bundle_id);
          }
          if ($onlyPublished) {
            $query->condition('status', 1);
          }
          $results = $query->execute();
          foreach ($results as $id) {
            foreach ($view_modes as $view_mode) {
              $this->itemIds[] = sprintf('%s:%s:%s', $entity_type_id, $id, $view_mode);
            }
          }
        }
      }
    }
    $cursor_position = is_null($cursor) ? -1 : array_search($cursor, $this->itemIds);
    if ($cursor_position === FALSE) {
      return [];
    }
    return array_slice($this->itemIds, $cursor_position + 1, (int) $this->getBatchSize());
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreConfigurationFormElements(array $form, SubformStateInterface $form_state): array {
    $options = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($entity_type->get('field_ui_base_route') && $entity_type->hasViewBuilderClass()) {
        $bundles = $this->bundleInfo->getBundleInfo($entity_type->id());
        $label = (string) $entity_type->getLabel();
        $entity_type_id = $entity_type->id();
        $options[$label] = [];
        foreach ($bundles as $bundle_id => $bundle_data) {
          $bundle_label = ($bundle_data['label'] instanceof TranslatableMarkup) ? $bundle_data['label']->render() : $bundle_data['label'];
          $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle_id);
          foreach ($view_modes as $view_mode_id => $view_mode_data) {
            $view_mode_label = ($view_mode_data instanceof TranslatableMarkup) ? $view_mode_data->render() : $view_mode_data;
            $options[$label][sprintf('%s:%s:%s', $entity_type_id, $bundle_id, $view_mode_id)] = $bundle_label . ' - ' . $view_mode_label;
          }
        }
      }
    }
    $configuration = $this->getConfiguration();
    $form['entity_view_modes'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity View Modes'),
      '#description' => $this->t('Enable the entity view modes to warm asynchronously.'),
      '#options' => $options,
      '#default_value' => empty($configuration['entity_view_modes']) ? [] : $configuration['entity_view_modes'],
      '#multiple' => TRUE,
      '#attributes' => ['style' => 'min-height: 60em;'],
    ];

    $form['published'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only warm published entities'),
      '#default_value' => $configuration['published'],
      '#description' => $this->t('If the entity type supports publishing.'),
    ];

    return $form;
  }

  /**
   * Returns configured view modes, grouped by bundle and entity type.
   *
   * @param array $configuration
   *   The configuration to use.
   */
  public function getViewModesFromConfig(array $configuration): array {
    $entity_view_modes = [];
    if (array_key_exists('entity_view_modes', $configuration)) {
      foreach ($configuration['entity_view_modes'] as $entity_bundle_view_mode) {
        [$entity_type, $bundle, $view_mode] = explode(':', $entity_bundle_view_mode);
        $entity_view_modes[$entity_type][$bundle][] = $view_mode;
      }
    }

    return $entity_view_modes;
  }

}
